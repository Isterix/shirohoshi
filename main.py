import discord
import requests
import json
import enum
import random
from datetime import datetime


#-----BOT HANDLER-----

class MyClient(discord.Client): #Creates the connection between host and bot
	async def on_ready(self): #Checks the connection between host and bot
		print('\nLogged on as {0}!'.format(self.user))
		print(datetime.now())
		status = await status_setter()
		await shirohoshi.change_presence(activity=discord.Game(status))

	async def on_message(self, message): #Checks for messages
		if str(message.author.id) != '{0}'.format(self.user.id): #If the message isn't from the bot
			if str(message.content)[0:len(prefix)] == prefix: #If the message seems to call the bot
				await command_finder(self, message)
	
#-----COSMETICS-----

async def status_setter():
	choice = random.randint(0, 20)
	if choice <= 15: return "shirohoshi-bot.carrd.co"
	if choice == 16: return 'Did you know that Shirohoshi means "White Star"?'
	if choice == 17: return "Did you know that Shirohoshi is open-source?"
	if choice == 18: return "Did you notice that Shirohoshi only refers to itself in third person?"
	if choice == 19: return "Did you know that English was not the originally intended main language for Shirohoshi?"
	if choice == 20: return "osu!ctb"
				
#-----MOD HANDLER-----
				
class Mods(enum.Flag):
	NF           = 1
	EZ           = 2
	TouchDevice  = 4
	HD           = 8
	HR           = 16
	SD           = 32
	DT           = 64
	RX           = 128
	HT           = 256
	NC           = 512     #Only set along with DoubleTime. i.e: NC only gives 576
	FL           = 1024
	Autoplay     = 2048
	SO           = 4096
	Autopilot    = 8192    #Also known as Relax2
	PF           = 16384   #Only set along with SuddenDeath. i.e: PF only gives 16416  
	Key4         = 32768
	Key5         = 65536
	Key6         = 131072
	Key7         = 262144
	Key8         = 524288
	FadeIn       = 1048576
	Random       = 2097152
	Cinema       = 4194304
	Target       = 8388608
	Key9         = 16777216
	KeyCoop      = 33554432
	Key1         = 67108864
	Key3         = 134217728
	Key2         = 268435456
	ScoreV2      = 536870912
	Mirror       = 1073741824
	KeyMod = Key1 | Key2 | Key3 | Key4 | Key5 | Key6 | Key7 | Key8 | Key9 | KeyCoop
	FreeModAllowed = NF | EZ | HD | HR | SD | FL | FadeIn | RX | Autopilot | SO | KeyMod
	ScoreIncreaseMods = HD | HR | DT | FL | FadeIn

async def moding(mods):
	mods = int(mods)
	if mods == 0:
		mods = "NM"
	else:
		mods = Mods(mods)
		mods = str(mods)
		mods = mods.replace("Mods.", "")
		if "NC" in mods: mods = mods.replace("DT", "")
		if "PF" in mods: mods = mods.replace("SD", "")
		if "||" in mods: mods = mods.replace("||", "")
		if "|" in mods: mods = mods.replace("|", "")
	return mods
	
async def star_rater(beatmap, mods):
	magic_number = 0
	if "EZ" in mods: magic_number = magic_number + 2
	if "HR" in mods: magic_number = magic_number + 16
	if "DT" in mods or "NC" in mods: magic_number = magic_number + 64
	if "HT" in mods: magic_number = magic_number + 256
	if magic_number != 0: #Only make necessary requests to save peppy's servers <3
		base = 'https://osu.ppy.sh/api/get_beatmaps?b=' + beatmap.json()[0]['beatmap_id']
		reste = '&mods=' + str(magic_number)
		response = await profiling(base, reste)
		return response.json()[0]['difficultyrating']
	else:
		return beatmap.json()[0]['difficultyrating']

#-----COMMAND HANDLER-----

async def command_finder(self, message):
	print("\n-----" + str(datetime.now()) + "-----")
	if str(message.guild) != "None": print("New command by " + str(message.author) + " on " + str(message.guild) + "'s '" + str(message.channel) + "' text channel: " + message.content)
	if str(message.guild) == "None": print("New command by " + str(message.author) + " in Private Messages: " + message.content)
	direct = await command_analyzer(self, message)
	if direct != "Error":
		if message.content[len(prefix):len(prefix) + 3] == "iam":
			await iam(self, message, direct)
		if message.content[len(prefix):len(prefix) + 4] == "best":
			await best(self, message, direct)
		if message.content[len(prefix):len(prefix) + 7] == "profile":
			await profile(self, message, direct)
		if message.content[len(prefix):len(prefix) + 5] == "image":
			await imaging(self, message, direct)
		if message.content[len(prefix):len(prefix) + 6] == "recent":
			await recent(self, message, direct)
		if message.content[len(prefix):len(prefix) + 4] == "help":
			await helping(self, message, direct)
	else:
		print("Too many spaces! (" + str(message.content.count(" ")) + ")")
		await message.channel.send("Shirohoshi cannot handle so many arguments!")
		print("-----" + str(datetime.now()) + "-----")
		
async def command_analyzer(self, message):
	if message.content.count(" ") == 0: return True
	if message.content.count(" ") >= 1 and message.content.count(" ") <= 2: return False
	if message.content.count(" ") > 2 and "best" in message.content: return False
	if message.content.count(" ") > 2: return "Error"

#-----CHECK OSU! ACCOUNT-----

async def checking(self, message, direct):
	user_id = False
	banned = False
	if direct == True:
		associations = open(text_file, "a+") #If file doesn't exist, this line creates it (important)
		with open(text_file, "r") as f:
			lines = f.readlines()
			for line in lines:
				if '{0.author.id}'.format(message) in line.strip("\n"):
					response = await profiling('https://osu.ppy.sh/api/get_user?u=', line[line.find("=") + 2:])
					if response.status_code == 200:
						result = json.dumps(response.json(), sort_keys=True, indent=4)
						if "username" in result:
							user_id = line[line.find("=") + 2:]
						else:
							banned = True
							print("The following account got associated before it disappeared: " + line[line.find("=") + 2:])
							await message.channel.send("<@" + '{0.author.id}'.format(message) + "> Shirohoshi believes your osu! account no longer exists, it most likely got suspended...")
					else:
						print("not 200")
			if user_id == False and banned == False:
				print("Checking has found no line that was related to this Discord Account ID: " + '{0.author.id}'.format(message))
				await message.channel.send("Your account has not been found in Shirohoshi's database.\nYou can link your osu! account with '" + prefix + "iam [nickname]'!")
	if direct == False:
		if message.content.count(" ") <= 1: response = await profiling('https://osu.ppy.sh/api/get_user?u=', message.content[message.content.find(" ") + 1:])
		if message.content.count(" ") >= 2: response = await profiling('https://osu.ppy.sh/api/get_user?u=', message.content[message.content.index(" ") + 1:message.content.rindex(" ")])
		if response.status_code == 200:
			result = json.dumps(response.json(), sort_keys=True, indent=4)
			if "username" in result:
				user_id = response.json()[0]['user_id']
			else:
				banned = True
				print("This account was not found by Shirohoshi on osu!'s API servers: " + message.content[len(prefix) + 6:])
				await message.channel.send("Shirohoshi could not find this account!")
	return user_id
	
#-----DEFINE OSU! ACCOUNT-----

async def iam(self, message, direct):
	if direct == False:
		response = await profiling('https://osu.ppy.sh/api/get_user?u=', message.content[len(prefix) + 4:])
		if response.status_code == 200:
			result = json.dumps(response.json(), sort_keys=True, indent=4)
			if "username" in result:
				id_of_user = response.json()[0]["user_id"]
				associations = open(text_file, "a+") #If file doesn't exist, this line creates it (important)
				with open(text_file, "r") as f:
					lines = f.readlines()
				with open(text_file, "w") as f:
					for line in lines:
						if '{0.author.id}'.format(message) not in line.strip("\n"):
							f.write(line)
						else:
							print("Removed line from " + text_file + ": " + line.strip("\n"))
				associations.write('{0.author.id}'.format(message) + " = " + id_of_user + '\n')
				print("Added line to " + text_file + ': {0.author.id}'.format(message) + " = " + id_of_user)
				await message.channel.send("<@" + '{0.author.id}'.format(message) + "> This is now the osu! account associated with your Discord account:\nhttps://osu.ppy.sh/users/" + id_of_user)
			else:
				print("osu! account not found on osu! API servers!")
				await message.channel.send("Shirohoshi could not find your osu! account...")
		else:
			print("osu! API servers did not respond with code 200, they are probably unavailable: " + response.status.code)
			print("For reference, here is response.json: " + response.json())
			await message.channel.send("The osu! servers seem to be unavailable for now. Shirohoshi thinks you should wait for a bit before trying again!")
	else:
		print("No (valid) argument was associated with " + prefix + "iam!")
		await message.channel.send(prefix + "iam has to be followed with an argument! e.g., " + prefix + "iam Isterix")
	print('-----' + str(datetime.now()) + '-----')

#-----GIVE OSU! PROFILE-----

async def profile(self, message, direct):
	associations = open(text_file, "a+") #If file doesn't exist, this line creates it (important)
	user_id = await checking(self, message, direct)
	if user_id != False:
		response = await profiling('https://osu.ppy.sh/api/get_user?u=', user_id)
		if response.status_code == 200:
			result = json.dumps(response.json(), sort_keys=True, indent=4)
			if "username" in result:
				print("osu! profile found in osu's database")
				profile_message = discord.Embed()
				username = response.json()[0]["username"]
				thumbnail = "http://s.ppy.sh/a/" + response.json()[0]["user_id"]
				country = response.json()[0]["country"]
				accuracy = response.json()[0]["accuracy"]
				rank = response.json()[0]["pp_rank"]
				country_rank = response.json()[0]["pp_country_rank"]
				pp = response.json()[0]["pp_raw"]
				level = response.json()[0]["level"]
				score_r = response.json()[0]["ranked_score"]
				score_t = response.json()[0]["total_score"]
				plays = response.json()[0]["playcount"]
				ss_plus = response.json()[0]["count_rank_ssh"]
				ss = response.json()[0]["count_rank_ss"]
				s_plus = response.json()[0]["count_rank_sh"]
				s = response.json()[0]["count_rank_s"]
				a = response.json()[0]["count_rank_a"]
				joined = response.json()[0]["join_date"]
				profile_message.title = username + "'s profile"
				desc = "Global rank: " + rank + " / " + country + " rank: " + country_rank + "\nLevel: " + level[:level.find(".")] + " / " + pp[:pp.find(".")] + "pp"
				desc_2 = "\n\nRanked score: " + score_r + " / Total score: " + score_t + "\nAccuracy: " + accuracy[:accuracy.find(".") + 3] + "% / Play Count: " + plays
				profile_message.description = desc + desc_2 + "\nSS+: " + ss_plus + " / SS: " + ss + "\nS+: " + s_plus + " / S: " + s + " / A: " + a
				profile_message.colour = discord.Colour(15073934)
				profile_message.set_footer(text="Joined osu! on " + joined + "!")
				profile_message.set_thumbnail(url = thumbnail)
				profile_message.url = "https://osu.ppy.sh/users/" + user_id
				await message.channel.send(embed = profile_message)
			else:
				print("osu! profile not found in osu's database")
				await message.channel.send("The osu! API servers did not respond with any data for your profile!")
		else:
			print("Request returned code: " + str(response.status_code))
			await message.channel.send("Shirohoshi could not access the osu! API servers...\n(did not receive code 200)")
	else:
		print("user_id returned false, so " + prefix + "profile was unsuccesful...")
	print("-----" + str(datetime.now()) + "-----")

#-----REQUEST FROM OSU! API SERVERS-----

async def profiling(base, osu_nick):
	global osu_key
	print("(PROFILING FUNCTION) Request at: " + base + osu_nick + '&k=' + "censored osu_key variable")
	res = requests.get(base + osu_nick + '&k=' + osu_key, timeout=None)
	return res
	
#-----GIVE OSU! PROFILE PICTURE-----

async def imaging(self, message, direct):
	user_id = await checking(self, message, direct)
	if user_id != False:
		user_url = "http://s.ppy.sh/a/" + user_id
		image = discord.Embed()
		image.set_image(url = user_url)
		print("(IMAGING FUNCTION) Sending image: " + user_url)
		await message.channel.send(embed = image)
	else:
		print("user_id returned false, so " + prefix + "image was unsuccesful...")
	print("-----" + str(datetime.now()) + "-----")

#-----GIVE OSU! BEST SCORES-----

async def best(self, message, direct):
	if message.content.count(" ") <= 1:
		nombre = message.content[len(prefix) + 5:len(prefix) + 7]
		direct = True
	if message.content.count(" ") >= 2: nombre = message.content[message.content.rindex(" ") + 1:]
	if nombre.isdigit():
		nombre = int(nombre)
		if nombre <= 10:
			user_id = await checking(self, message, direct)
			if user_id != False:
				colours = [15066597, 13421772, 11711154, 9934743, 8355711, 6776679, 5066061, 3487029, 1973790, 0] #Decimal values of colours for each play, from shiro to kuro
				numero = 0
				response = await profiling('https://osu.ppy.sh/api/get_user_best?u=', user_id)
				for score in range(nombre):
					best_message = discord.Embed()
					beatmap = await profiling('https://osu.ppy.sh/api/get_beatmaps?b=', response.json()[numero]['beatmap_id'])
					thumbnail = "https://b.ppy.sh/thumb/" + beatmap.json()[0]['beatmapset_id'] + ".jpg"
					artist = beatmap.json()[0]['artist']
					title = beatmap.json()[0]['title']
					beatmapper = beatmap.json()[0]['creator']
					difficulty = beatmap.json()[0]['version']
					max_combo = beatmap.json()[0]['max_combo']
					sr_base = beatmap.json()[0]['difficultyrating']
					pp = response.json()[numero]['pp']
					play_date = response.json()[numero]['date']
					play_combo = response.json()[numero]['maxcombo']
					misses = response.json()[numero]['countmiss']
					trois = response.json()[numero]['count300']
					cent = response.json()[numero]['count100']
					cinquante = response.json()[numero]['count50']
					score = response.json()[numero]['score']
					mods = response.json()[numero]['enabled_mods']
					mods = await moding(mods)
					sr_new = await star_rater(beatmap, mods)
					rating = response.json()[numero]['rank']
					if rating == "SH": rating = "S+"
					if rating == "X": rating = "SS"
					if rating == "XH": rating = "SS+"
					best_message.title = artist + " - " + title
					if sr_base == sr_new: desc = rating + " rank, " + pp[:pp.find(".")] + "pp on " + difficulty + " (" + mods + ")\n**" + sr_base[:sr_base.find(".") + 2] + " stars**\n" + play_combo + "x/" + max_combo + "x, " + misses + " misses"
					if sr_base != sr_new: desc = rating + " rank, " + pp[:pp.find(".")] + "pp on " + difficulty + " (" + mods + ")\n" + sr_base[:sr_base.find(".") + 2] + " stars --> **" + sr_new[:sr_new.find(".") + 2] + " stars**\n" + play_combo + "x/" + max_combo + "x, " + misses + " misses"
					best_message.description = desc + "\n300s: " + trois + "\n100s: " + cent + "\n50s: " + cinquante
					best_message.set_footer(text="Beatmap by " + beatmapper + " played on " + play_date)
					best_message.colour = discord.Colour(colours[numero])
					best_message.set_thumbnail(url = thumbnail)
					best_message.url = "https://osu.ppy.sh/beatmapsets/" + beatmap.json()[0]['beatmapset_id']
					await message.channel.send(embed = best_message)
					print("Score sent successfully!")
					numero = numero + 1
			else:
				print("user_id returned false, so " + prefix + "best was unsuccesful...")
		else:
			print(prefix + "best was followed by a number that was too big")
			await message.channel.send("You may use this command followed by a number that is from 1 to 10 only! " + str(nombre) + " is too big...")
	else:
		print(prefix + "best was not used correctly")
		await message.channel.send("The command did not work as Shirohoshi was expecting either of those syntaxes:\n" + prefix + "best [number from 1-10]\n" + prefix + "best [nickname] [number from 1-10]")
	print("-----" + str(datetime.now()) + "-----")

#-----GIVE OSU! RECENT SCORE-----

async def recent(self, message, direct):
	user_id = await checking(self, message, direct)
	if user_id != False:
		response = await profiling('https://osu.ppy.sh/api/get_user_recent?u=', user_id)
		result = json.dumps(response.json(), sort_keys=True, indent=4)
		if "score" in result:
			recent_message = discord.Embed()
			beatmap = await profiling('https://osu.ppy.sh/api/get_beatmaps?b=', response.json()[0]['beatmap_id'])
			thumbnail = "https://b.ppy.sh/thumb/" + beatmap.json()[0]['beatmapset_id'] + ".jpg"
			artist = beatmap.json()[0]['artist']
			title = beatmap.json()[0]['title']
			beatmapper = beatmap.json()[0]['creator']
			difficulty = beatmap.json()[0]['version']
			max_combo = beatmap.json()[0]['max_combo']
			sr_base = beatmap.json()[0]['difficultyrating']
			play_date = response.json()[0]['date']
			play_combo = response.json()[0]['maxcombo']
			misses = response.json()[0]['countmiss']
			trois = response.json()[0]['count300']
			cent = response.json()[0]['count100']
			cinquante = response.json()[0]['count50']
			score = response.json()[0]['score']
			mods = response.json()[0]['enabled_mods']
			mods = await moding(mods)
			sr_new = await star_rater(beatmap, mods)
			rating = response.json()[0]['rank']
			if rating == "SH": rating = "S+"
			if rating == "X": rating = "SS"
			if rating == "XH": rating = "SS+"
			recent_message.title = artist + " - " + title
			if sr_base == sr_new: desc = rating + " rank on " + difficulty + " (" + mods + ")\n**" + sr_base[:sr_base.find(".") + 2] + " stars**\n" + play_combo + "x/" + max_combo + "x, " + misses + " misses"
			if sr_base != sr_new: desc = rating + " rank on " + difficulty + " (" + mods + ")\n" + sr_base[:sr_base.find(".") + 2] + " stars --> **" + sr_new[:sr_new.find(".") + 2] + " stars**\n" + play_combo + "x/" + max_combo + "x, " + misses + " misses"
			recent_message.description = desc + "\n300s: " + trois + "\n100s: " + cent + "\n50s: " + cinquante
			recent_message.colour = discord.Colour(20941)
			recent_message.set_footer(text="Beatmap by " + beatmapper + " played on " + play_date)
			recent_message.url = "https://osu.ppy.sh/beatmapsets/" + beatmap.json()[0]['beatmapset_id']
			recent_message.set_thumbnail(url = thumbnail)
			await message.channel.send(embed = recent_message)
			print("Score sent successfully!")
		else:
			print("No score was available, so " + prefix + "recent did not work!")
			await message.channel.send("No play was made recently...")
	else:
		print("user_id returned false, so " + prefix + "recent was unsuccesful...")
	print("-----" + str(datetime.now()) + "-----")
	
#-----HELPING THE USER OUT-----

async def helping(self, message, direct):
	if direct == True: await message.channel.send("You can get everything you want from Shirohoshi on its website!\nhttps://shirohoshi-bot.carrd.co")
	if direct != True:
		if "iam" in str(message.content): await message.channel.send(prefix + "iam [nickname] is a command that makes interacting with the bot easier!\nWhen you use other commands, you may not tell the bot a nickname, in which case it will use the nickname you told it with this command.")
		if "best" in str(message.content): await message.channel.send(prefix + "best {nickname] [number] is a command to get the top [number] plays of a player.\nYou may use " + prefix + "best Vaxei 3 to get Vaxei's top 3 plays, for example!")
		if "recent" in str(message.content): await message.channel.send(prefix + "recent [nickname] is a command to get someone's most recent play!\nThat play must be from less than 24h ago.")
		if "profile" in str(message.content): await message.channel.send(prefix + "profile [nickname] is a command that gives you details about someone's osu! account that can only be found there.")
		if "image" in str(message.content): await message.channel.send(prefix + "image [nickname] gives you the profile picture of an osu! account! Go put some waifus in that channel!")
	print(prefix + "helping seems to have worked!")
	print("-----" + str(datetime.now()) + "-----")

#-----DEFINE VARIABLES & RUN CODE-----
	
prefix = ')'
text_file = 'associations' + '.txt' #File that makes Discord accounts and osu! accounts linked
osu_key = '' #osu! Token goes here
shirohoshi = MyClient()
shirohoshi.run('') #Discord Token goes here
