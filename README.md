# Shirohoshi

## Introduction

Shirohoshi is a Discord bot that gets all the information you want from the osu! API (v1). Use simple commands to get all the information you want on your Discord server whenever you wish!

You can find everything you need to use Shirohoshi on its website: https://shirohoshi-bot.carrd.co/

## Making your own Shirohoshi

If you wish to run your own version of Shirohoshi, let me help you do that!

To run the code, you will need:

* [Python 3](https://www.python.org/)
* [discord.py](https://discordpy.readthedocs.io/en/latest/)
* [Requests](https://2.python-requests.org/en/master/)

To make the code do things, you will need:

* [A Discord bot](https://discordapp.com/developers/applications)
* [An osu! API Key](https://osu.ppy.sh/p/api/)

In main.py, find the osu_key variable and put your osu! API Key between the ''. Also, put the Discord Token of your bot between the '' of the file's last line!

You may then do whatever change you want to main.py. Once you're ready, you can execute that file! I recommend using cmd.exe for that just in case there's an error.

## License

This project is licensed under the GNU General Public License - see the LICENSE file for details
